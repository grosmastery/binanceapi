import plotly.graph_objects as go
import pandas as pd

from flask import Flask, render_template

app = Flask(__name__)


def candlestick():
    df = pd.read_csv("binance_csv.csv")

    fig = go.Figure(
        data=[go.Candlestick(x=df['time'], open=df['open'], high=df['high'], low=df['low'], close=df['close'])])
    fig.update_layout(xaxis_rangeslider_visible=False)
    fig.update_layout(height=600, width=1200)
    return fig


def pie_chart():
    symbols = ['AAPL', 'GOOGL', 'MSFT', 'AMZN', 'FB', 'TSLA', 'NVDA', 'JPM', 'V', 'JNJ']
    market_caps = [2295, 1845, 1830, 1730, 895, 682, 602, 492, 469, 434]

    fig = go.Figure(data=[go.Pie(labels=symbols, values=market_caps)])
    return fig


@app.route('/')
def main():

    candlestick_fig = candlestick()
    candlestick_fig_html = candlestick_fig.to_html(full_html=False, default_height=500, default_width=700)

    pie_fig = pie_chart()
    pie_fig_html = pie_fig.to_html(full_html=False, default_height=500, default_width=700)

    return render_template('html_page.html', candlestick_fig_html=candlestick_fig_html,  pie_fig_html=pie_fig_html)


if __name__ == '__main__':
    app.run(debug=True)
