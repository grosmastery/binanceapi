## **Getting Started**
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### **Prerequisites**
The project is developed using Python. You need to have Python installed on your machine.

This project also requires the following Python libraries:

> - Flask
> - Plotly
> - Pandas
> - Binance-connector

### **Project Structure**
The project comprises three main files:

`flask_file.py`: This is the main Flask application file. It has two functions candlestick() and pie_chart() for creating Candlestick and Pie Chart respectively using Plotly. The main() function is the route handler for the root URL of the web application. It generates HTML for the charts using the aforementioned functions and injects them into the HTML template.

`main.py`: This file handles fetching of kline data from the Binance API, processes the data and writes it to a CSV file. The file runs in a continuous loop, fetching new data every hour and appending it to the CSV file.

`html_page.html`: This is the HTML template for the web application. It receives the Plotly chart HTML from the Flask application and inserts it into appropriate places in the HTML structure.

### **Usage**
When you start the Flask application, it will serve a webpage at `http://localhost:5000`. The page displays a Candlestick Chart of Binance data for the BTCUSDT pair and a Pie Chart showing market caps of ten predefined symbols. The Candlestick Chart is updated every hour with new data from Binance.
