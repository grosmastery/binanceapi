import time
import csv
import sqlite3
from binance.spot import Spot
import datetime


class BinanceKlines:
    def __init__(self, clients, symbol_filr, interval):
        self.client = clients
        self.symbol = symbol_filr
        self.interval = interval

    def get_klines(self):
        return self.client.klines(self.symbol, self.interval)

    def get_klines_data(self, get_klines):
        data_list = []
        for i in get_klines:
            open_time = datetime.datetime.fromtimestamp(int(i[0]) / 1000)
            open_val, high, low, close = map(float, (i[1], i[2], i[3], i[4]))
            data_list.append([open_time, open_val, high, low, close])
        return data_list

    def save_klines_data_csv(self, data_list, write):
        write.writerows(data_list)

    # # sqlite save data
    # def save_klines_data_db(self, data_list):
    #     db_name = 'binance_db.sqlite'
    #     conn = sqlite3.connect(db_name)
    #     cursor = conn.cursor()
    #
    #     cursor.execute('''CREATE TABLE IF NOT EXISTS binance_data
    #                      (time DATETIME, open INT, high INT, low INT, close INT)''')
    #
    #     cursor.executemany('INSERT INTO binance_data VALUES (?,?,?,?,?)', data_list)
    #
    #     conn.commit()
    #     conn.close()

    def main(self, write):
        get_klines = self.get_klines()
        data_list = self.get_klines_data(get_klines)
        self.save_klines_data_csv(data_list, write)
        print(f"{self.symbol} data was saved")

        # # sqlite save data
        # self.save_klines_data_db(data_list)
        # print(f"{self.symbol} data was saved")


if __name__ == "__main__":
    client = Spot()
    symbol = "BTCUSDT"
    filename = "binance_csv.csv"
    with open(filename, "w", encoding="utf-8") as f:
        headers = ["time", "open", "high", "low", "close"]
        writer = csv.writer(f)
        writer.writerow(headers)
        while True:
            b = BinanceKlines(client, symbol, "1h")
            b.main(writer)
            time.sleep(3600)
